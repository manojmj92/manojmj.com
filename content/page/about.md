---
title: About me
# subtitle: Why you'd want to hang out with me
comments: false
---

I'm Manoj. I work at GitLab.

- I love Ruby
- I love boring code
- I love helping people in their software engineering career.

If you'd like any help from me, hit me up on Twitter!
